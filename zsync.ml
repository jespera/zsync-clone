exception Not_implemented

(* Utility functions *)


(* Given list of length >= n, returns the first n elements of list *)
let rec take n xs = 
  match n, xs with 
  | 0, xs -> [] 
  | n, [] -> failwith "take" 
  | n, x::xs -> x :: take (n-1) xs 

(* Converts given string to list of chars *)
let explode s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) ((Char.code s.[i]) :: l) in
  exp (String.length s - 1) []

(* Converts list of chars to string *)
let implode l =
  let res = String.create (List.length l) in
  let rec imp i = function
    | [] -> res
    | c :: l -> res.[i] <- (Char.chr c); imp (i + 1) l in
  imp 0 l
    
(* Given list of length >= n, returns a pair of the first n elements
   of list and the rest of the list *)
let rec take_drop n xs =
  match n, xs with 
  | 0, _ -> [], xs
  | n, [] -> failwith ("take_drop " ^ string_of_int n)
  | n, x :: xs -> 
    let r, ys = take_drop (n - 1) xs in x :: r, ys



type weak_hash = int
type strong_hash = string
type interval = int * int

let p_addler = 65521 (* largest primer < 2^16 *)

let (+>) o f = f o

let a xs = 
  xs 
  +> List.fold_left (fun acc x -> acc + x) 1
  +> fun x -> x mod p_addler

let b xs =
  let n = List.length xs in
  xs 
  +> List.fold_left (fun (acc, i) x -> (n-i+1)*x + acc, i + 1) (0,1)
  +> fun (sum, _) -> (sum + n) mod p_addler

let up x = if x >= 0 then x else x + p_addler

let a_add old_a add_val = (old_a + add_val) mod p_addler +> up
let a_rm  old_a rm_val  = (old_a - rm_val) mod p_addler +> up

let b_add old_a old_b add_val   = old_a + old_b + add_val +> up
let b_rm  old_b old_size rm_val = old_b - 1 - (old_size * rm_val) +> up

let init_a_b = (1, 0)

let s xs  = a xs + 65536 * b xs 
let compute_weak (a,b) = (b lsl 16) lor a

type block =
| Block of weak_hash * strong_hash * interval

type 'a instruction =
| Copy of 'a list
| Fetch of block

(* Find a block matching sign in list of blocks by linear search from
   first to last *)
let find_block (weak, sign) blocks = List.find (fun (Block(w,s,i)) -> weak = w && s = sign) blocks 

let string_of_a_b (a,b) = "("^string_of_int a^","^string_of_int b^")"

let cap_window_size b_size bytes = min (List.length bytes) b_size

let slide_1 (a,b) window_size bytes =
  if window_size >= (List.length bytes)
  then (* we are in the 'remove from front' case *)
    match bytes with
    | [] -> failwith "slide_1: bytes = [] && window_size = 0"
    | x :: _ -> (a_rm a x, b_rm b window_size x)
  else (* we are in 'remove and add new' case *)
    match bytes with
    | [] -> failwith "slide_1: bytes = [] && window_size > 0"
    | x_out :: _ -> let window, rest = take_drop window_size bytes in
		    (match rest with
		    | [] -> failwith "slide_1: window_size < |bytes| but byte-window =[]?"
		    | x_in :: _ -> let a' = a_rm a x_out in
				   (a_add a' x_in,
				    b_add a' (b_rm b window_size x_out) x_in)
		    )
		
let slide_n orig_n (a,b) window_size bytes =
  let rec loop n bytes a_b = 
    if n = 0 then a_b
    else match bytes with
    | [] -> failwith ("slide_n: empty list with n = " ^ string_of_int n)
    | b :: ytes -> 
        slide_1 a_b (cap_window_size window_size bytes) bytes
        +> loop (n - 1) ytes 
  in loop orig_n bytes (a,b)

let take_bytes b_size bytes = take_drop (cap_window_size b_size bytes) bytes

let rec get_next_matching_block b_size our_bytes a_b_orig blocks = 
  (* scan our_bytes until match is found in blocks *)
  (* return None if no mathes are found  *)
  (* otherwise return matching block + suffix of our_bytes where match
     started + (a,b) pair for window starting a suffix *)
  let rec loop a_b our_bytes =
    if List.length our_bytes = 0 then None
    else
      let rem_size = cap_window_size b_size our_bytes in
      let bytes = take rem_size our_bytes in
      let (weak, sign)  = compute_weak a_b, Digest.string (implode bytes) in
      try
	(* find sign in blocks *)
	let block = find_block (weak, sign) blocks in
	(* if found, return block, our_bytes *)
	Some (block, our_bytes, a_b)
      with Not_found ->
	loop (slide_1 a_b rem_size our_bytes) (List.tl our_bytes) 
  in
  loop a_b_orig our_bytes
        
let rec update b_size (a_b, our_bytes) remote_blocks =
  match remote_blocks with
  | [] -> []
  | b :: bs -> 
    (match get_next_matching_block b_size our_bytes a_b remote_blocks with
    | None -> List.map (fun block -> Fetch block) remote_blocks
    | Some (mb, remaining_bytes, new_a_b) -> 
      if mb = b then 
	let bytes, rest_bytes = take_bytes b_size remaining_bytes in
	let window_size = List.length bytes in 
	let newer_a_b = slide_n window_size
	                        new_a_b
				window_size
				remaining_bytes in
	Copy bytes :: 
	  update b_size 
	         (newer_a_b, rest_bytes) 
                 bs
      else
	Fetch b :: update b_size (a_b, our_bytes) bs
    )
    
let rec compute_blocks b_size bytes =
  let rec loop a_b offset remaining_bytes =
    if remaining_bytes = [] then []
    else let head, rest_bytes = take_bytes b_size remaining_bytes in
	 let window_size = List.length head in (* could be < b_size for last block *)
	 let new_a_b = slide_n window_size a_b window_size remaining_bytes in
	 let block = Block (s head (* OR equivalent: compute_weak a_b *),
			    Digest.string (implode head), 
			    (offset, offset + window_size - 1)) 
	 in block :: loop new_a_b (offset + window_size) rest_bytes in
  let first_w_size = cap_window_size b_size bytes in
  let first_block = take first_w_size bytes in
  let first_a_b = (a first_block, b first_block) in
  loop first_a_b 0 bytes


let l2    = [2]
let l12   = [1;2]
let l23   = [2;3]
let l123  = [1;2;3]
let l1234 = [1;2;3;4]
let l234  = [2;3;4]
let l34   = [3;4]
let l4    = [4]

let test_a_add = a l123 = a_add (a l12) 3
let test_a_rm  = a l23 = a_rm (a l123) 1

let test_a_add_rm = a l23 = a_rm (a_add (a l12) 3) 1
let test_a_rm_add = a l23 = a_add (a_rm (a l12) 1) 3

let test_b_add = b l123 = b_add (a l12) (b l12) 3
let test_b_rm  = b l23 = b_rm (b l123) 3 1

let test_b_add_rm = b l23 = b_rm (b_add (a l12) (b l12) 3) 3 1
let test_b_rm_add = b l23 = b_add (a l2) (b_rm (b l12) 2 1) 3

let test_compute_weak = s l123 = compute_weak (a l123, b l123)
let slide_1_val = slide_1 (a l12, b l12) 2 l123
let test_slide_1 = (a l23, b l23) = slide_1_val
let test_slide_1_slide_1 = (a l34, b l34) = slide_1 slide_1_val 2 l234
let test_slide_1_out = (a l34, b l34) = slide_1 (a l234, b l234) 3 l234

(* slide_n n (a,b) window_size bytes *)
let test_slide_n_1     = (a l23, b l23) = slide_n 1 (a l12, b l12) 2 l1234
let test_slide_n_2     = (a l34, b l34) = slide_n 2 (a l12, b l12) 2 l1234
let test_slide_n_3_out = (a l4, b l4)   = slide_n 3 (a l12, b l12) 2 l1234

let bls = compute_blocks 4 (explode "hej med dig due der")
let loc = explode "med dig due der"

let loc' =
  let w_size = cap_window_size 4 loc in
  let block  = take w_size loc in
  let a_b    = (a block, b block) in
  update 4 (a_b, loc) bls

let compress_instructions ins =
  let add_ins acc_ins inst =
    match inst with
    | Fetch(Block(_,_,(i_start,i_end))) -> 
      (match acc_ins with
      | Fetch(Block(w,s,(a_start,a_end))) :: rest 
	  when a_end + 1 = i_start (* we visit instructions in order *)
	  -> Fetch(Block(w,s,(i_start, a_end))) :: rest
      | _ -> inst :: acc_ins
      )
    | _ -> inst :: acc_ins in
  List.fold_left add_ins [] ins
  +> List.rev

let _ = List.iter (fun x -> match x with
  | Copy chs -> print_endline ("Copy:  " ^ implode chs)
  | Fetch (Block (_,s,_)) -> print_endline ("Fetch: " ^ Digest.to_hex s))
  (compress_instructions loc')


